import ch.qos.logback.classic.Level
import org.cometd.client.BayeuxClient
import org.cometd.client.BayeuxClient.State.CONNECTED
import org.cometd.client.transport.ClientTransport.MAX_NETWORK_DELAY_OPTION
import org.cometd.client.transport.LongPollingTransport
import org.eclipse.jetty.client.HttpClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*


fun main(args: Array<String>) {
    val root = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME) as ch.qos.logback.classic.Logger
    root.level = Level.INFO
    val httpClient = HttpClient()
    httpClient.start()
    val client = BayeuxClient("http://localhost:8080/devicecontrol/notifications", LongPollingTransport(HashMap(mapOf(MAX_NETWORK_DELAY_OPTION to 320000)) as Map<String, Any>?, httpClient))
    client.handshake()
    client.waitFor(20, CONNECTED);
    client.getChannel("/1111").subscribe { clientSessionChannel, message ->
            println(message)
    }
}
