package com.cumulocity.device.push

import com.hazelcast.core.MultiMap
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.request.async.DeferredResult
import java.util.*


@RestController
class DevicePushController(val sessions: MultiMap<String, String>) {

    val log: Logger = LoggerFactory.getLogger(DevicePushController::class.java)

    val awaiting: MutableMap<String, MutableCollection<DeferredResult<Array<Message>>>> = mutableMapOf()

    @PostMapping("/devicecontrol/operations")
    fun create(@RequestBody operation: Map<String, Any?>) {
        val channel = "/" + operation["deviceId"]
        awaiting.get(channel)?.dropWhile {
            it.setResult(arrayOf(Messages.of(channel = channel, data = operation), Messages.of(channel = "/meta/connect", successful = true)))
            true
        }
    }

    @GetMapping("/sessions")
    fun sessions(): Map<String, Collection<String>> {
        return sessions.entrySet().groupByTo(mutableMapOf()) {
            it.key
        }.mapValues { it.value.map { it.value } }
    }

    @GetMapping("/listeners")
    fun listeners(): Map<String, Int> {
        return awaiting.map { it.key.to(it.value.size) }.associateBy({ it.first }).mapValues { it.value.second }
    }

    @PostMapping("/devicecontrol/notifications", "/devicecontrol/notifications/**")
    fun connect(@RequestBody messages: Array<Message>): DeferredResult<Array<Message>> {

        val result = DeferredResult<Array<Message>>()
        messages.forEach { connect ->
            log.info("connect {}", connect)
            when (connect.channel) {
                "/meta/handshake" -> {

                    val clientId = UUID.randomUUID().toString()

                    sessions.put(clientId, "")

                    result.setResult(arrayOf(Messages.handshake(successful = true, channel = connect.channel, clientId = clientId)))
                }
                "/meta/subscribe" -> {

                    sessions.put(connect.clientId, connect.subscription!!)
                    result.setResult(arrayOf(Messages.of(successful = true, channel = connect.channel, subscription = connect.subscription)))
                }
                "/meta/connect" -> {
                    sessions[connect.clientId].filter { !it.isBlank() }.forEach {
                        synchronized(awaiting) {
                            val watings = awaiting.getOrPut(it, { mutableListOf() })
                            watings.add(result)
                        }
                    }
                    result.onTimeout {
                        result.setResult(arrayOf(Messages.of(successful = true, channel = connect.channel, clientId = connect.clientId)))
                    }

                }
                else -> result.setResult(arrayOf(Messages.of(successful = false, channel = connect.channel, error = "unknown channel")))
            }

            result.onCompletion {
                if(result.result is Array<*>) {
                    @Suppress("UNCHECKED_CAST")
                    val response = result.result as Array<Message>
                    log.info("request completed for $connect with Result ${response.joinToString { it.toString() }}")
                }
                synchronized(awaiting) { awaiting.forEach { it.value.remove(result) } }
            }
        }
        return result
    }


}
