package com.cumulocity.device.push

import java.io.Serializable


class Session(val subscriptions: MutableList<String> = arrayListOf()) : Serializable {
}
