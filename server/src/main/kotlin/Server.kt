package com.cumulocity.device.push

import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.IMap
import com.hazelcast.core.MultiMap
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean


@SpringBootApplication
open class DevicePushApplication {
    @Bean
    open fun hazelcast(): HazelcastInstance {
        return Hazelcast.newHazelcastInstance()
    }

    @Bean
    open fun kotlinjacksonModule(): KotlinModule {
        return KotlinModule()
    }

    @Bean
    open fun sessions(instance: HazelcastInstance): MultiMap<String, String> {
        return instance.getMultiMap("device-push-sessions")
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(DevicePushApplication::class.java, *args)
}
