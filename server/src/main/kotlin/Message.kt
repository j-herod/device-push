package com.cumulocity.device.push

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL

@JsonInclude(NON_NULL)
data class Message @JsonCreator constructor(
        var channel: String?,
        val successful: Boolean?,
        val clientId: String?,
        val subscription: String?,
        val data: Any?,
        val error: String?,
        val minimulVersion: String?,
        val supportedConnectionTypes: Array<String>?)


object Messages {
    fun of(
            channel: String? = null,
            successful: Boolean? = null,
            clientId: String? = null,
            subscription: String? = null,
            data: Any? = null,
            error: String? = null): Message {
        return Message(channel, successful, clientId, subscription, data, error, null, null)
    }

    fun handshake(channel: String? = null,
                  successful: Boolean? = null,
                  clientId: String? = null): Message {
        return Message(channel, successful, clientId, null, null, null, "1.0", arrayOf("long-polling"))
    }
}
